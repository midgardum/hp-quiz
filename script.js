// Thanks to EventListener content will load first
// Penned name will be added to submit button and results
var name = prompt('Pen your name, please');
document.addEventListener('DOMContentLoaded',
    function() { 
        if (name == 'null' || name == '') {
            name = "Stranger" ;
            document.getElementById('submit').setAttribute('value', 'Get results, Stranger!');
        } else {
            document.getElementById('submit').setAttribute('value', 'Get results, ' + name + '!'); 
        }
    }
);

function submitAnswers() {
    
    // Access questions using document.forms['formName']['inputName].value 
    var q1 = document.forms['quiz']['question1'].value;
    var q2 = document.forms['quiz']['question2'].value;
    var q3 = document.forms['quiz']['question3'].value;
    var q4 = document.forms['quiz']['question4'].value;
    var q5 = document.forms['quiz']['question5'].value;
    var q6 = document.forms['quiz']['question6'].value;
    var q7 = document.forms['quiz']['question7'].value;
    var q8 = document.forms['quiz']['question8'].value;
    var q9 = document.forms['quiz']['question9'].value;
    var q10 = document.forms['quiz']['question10'].value;
   
    // Array with correct answers
    var answers = [
        "answer2for1",
        "answer4for2",
        "answer1for3",
        "answer2for4",
        "answer3for5",
        "answer2for6",
        "answer3for7",
        "answer4for8",
        "answer1for9",
        "answer4for10"
        ];  
    
    // Validate using eval function
    // Loop for will check each variable diefined above by adding 1 to q (var q1). If it's equal to null or has '' the alert will show.
    var total = 10;
    for (var i = 1; i <= total; i++) {
        if (eval('q' + i) === null || eval('q' + i) == '' ) {
            alert('You missed question ' + i);
            return false;
        }
    }
    
    // Count the score using eval function
    // The loop for will add 1 point if the array matches. The loop will be executed until it reaches value assigned in total variable (i <= total).
    var score = 0;
    for (var i = 1; i <= total; i++) {
        if (eval('q' + i) == answers[i - 1]) {
            score++;
        }
    }

    // Level of the score. Assigned varables with images
    var hermione = "<br><img src=\"images/hermione.png\">";
    var albus = "<br><img src=\"images/dumbledore.png\">";
    var draco = "<br><img src=\"images/draco.png\">";
    var troll = "<br><img src=\"images/troll.png\">";
    var snape = "<br><img src=\"images/snape.png\">";
    var level = "";
    switch (score) {
        case 10: case 9: level = "Excellent! You're a master of the Harry Potter universe!" + hermione; break;
        case 8: case 7: level = "Well done! You're not an expert, but you know a lot." + albus; break;
        case 6: case 5: level = "You don't know much about Harry Potter. Sorry." + draco; break;
        case 4: case 3: level = "Troll! You should read Harry Potter books again." + troll; break
        case 2: case 1: level = "Have you even read Harry Potter books?" + snape; break;
        default: level = "Shoo! Don't come back until you read Harry Potter!"; break;
    }
            
    // Display results
    document.getElementById('results').innerHTML = '<p id="name">' + name + '</p><h3>You scored <span>' + score + '</span> out of <span>' + total + '</span><br>' + level +'</h3>';
    
    // Array with explanations for questions
    var quotes = [
        "<p>It was a tricky question. Although we don’t know much about Fidelius Charm and it’s possible it can be broken by a caster himself, we know for sure that it stopped working after Peter Pettigrew, who was a Secret Keeper, had told Voldemort where the Potters resided. It seems that the purpose of the charm ends if there’s no secret to tell or if there’s nothing to hide (the death of the subjects of the spell). Also, the Fidelius Charm doesn’t stop working with the death of the Secret Keeper.</p><blockquote>And along a new and darker street he [Voldemort] moved, and now his destination was in sight at last, the Fidelius Charm broken, though they did not know it yet...</blockquote><blockquote>Mr. Weasley had explained that after the death of Dumbledore, their Secret-Keeper, each of the people to whom Dumbledore had confided Grimmauld Place’s location had become a Secret-Keeper in turn.</blockquote>",
        "<p>Unfortunately for ministry workers all three of them.</p><blockquote>Feeling exceptionally foolish, Harry clambered into the toilet. He knew at once that he had done the right thing; though he appeared to be standing in water, his shoes, feet, and robes remained quite dry. He reached up, pulled the chain, and next moment had zoomed down a short chute, emerging out of a fireplace into the Ministry of Magic.</blockquote><blockquote>A cool female voice sounded inside the telephone box (...).<br>\“Welcome to the Ministry of Magic. Please state your name and business.'</blockquote>",
        "<p>Although Severus Snape, who was teaching Potions, looked like a bat, he wasn’t a vampire and Remus Lupin was actually a werewolf. Vampires can be dangerous, because students learn how to deal with them, but some of them can be friends and thus be invited to parties.</p><blockquote>Slughorn led him purposefully into the party (...).<br> \“Harry, I'd like you to meet Eldred Worple, an old student of mine, author of 'Blood Brothers: My Life Amongst the Vampires' — and, of course, his friend Sanguini.<br> \“Worple, who was a small, stout, bespectacled man, grabbed Harry's hand and shook it enthusiastically; the vampire Sanguini, who was tall and emaciated with dark shadows under his eyes, merely nodded. He looked rather bored.</blockquote>",
        "<p>Although Horace Slughorn broke into someone’s house to live there and liked eating candied pineapples, the correct answer is the second one. He created a wreckage in the living room and hid himself as an armchair, hoping the headmaster would think he was dead.</p><blockquote> \“Maybe there was a fight and — and they dragged him off, Professor?\” Harry suggested, trying not to imagine how badly wounded a man would have to be to leave those stains spattered halfway up the walls.<br> \“I don't think so,\” said Dumbledore quietly, peering behind an overstuffed armchair lying on its side.<br> \“You mean he's — ?\”<br> \“Still here somewhere? Yes.\”<br>And without warning, Dumbledore swooped, plunging the tip of his wand into the seat of the overstuffed armchair, which yelled, \“Ouch!\”<br> \“Good evening, Horace,\” said Dumbledore, straightening up again.</blockquote>",
        "<p>There were white peacocks in Malfoy’s garden.</p><blockquote>There was a rustle somewhere to their right: Yaxley drew his wand again, pointing it over his companion’s head, but the source of the noise proved to be nothing more than a pure-white peacock, strutting majestically along the top of the hedge.<br> \“He always did himself well, Lucius. Peacocks...\” Yaxley thrust his wand back under his cloak with a snort.</blockquote>",
        "<p>Albus Dumbledore worked behind the scene, manipulated a lot and had a bit dark past, but at the same time he fought against evil and essentially was a good character.</p><blockquote>\“He changed, Harry, he changed! It’s as simple as that! Maybe he did believe these things when he was seventeen, but the whole of the rest of his life was devoted to fighting the Dark Arts! Dumbledore was the one who stopped Grindelwald, the one who always voted for Muggle protection and Muggle-born rights, who fought You-Know-Who from the start, and who died trying to bring him down! (...) I’m sorry, but I think the real reason you’re so angry is that Dumbledore never told you any of this himself.\”</blockquote>",
        "<p>The correct answer is the third one.</p><blockquote>He found himself looking at a number of flurry black creatures with long snouts. Their front paws were curiously flat, like spades, and they were blinking up at the class, looking politely puzzled at all the attention.<br> \“These're nifflers,\” said Hagrid, when the class had gathered around. \“Yeh find'em down mines mostly. They like sparkly stuff.\”</blockquote>",
        "<p>By mistake Hermione obtained a hair of Bulstrode’s cat.</p><blockquote>Her face was covered in black fur. Her eyes had turned yellow and there were long, pointed ears poking through her hair.<br> \“It was a c-cat hair!\” she howled. \“M-Millicent Bulstrode m-must have a cat! And the p-potion isn't supposed to be used for animal transformations!\”</blockquote>",
        "<p>Sadly, one of them died and it was Fred.</p><blockquote>The dead lay in a row in the middle of the Hall. Harry could not see Fred’s body, because his family surrounded him. George was kneeling at his head; Mrs. Weasley was lying across Fred’s chest, her body shaking, Mr. Weasley stroking her hair while tears cascaded down his cheeks.</blockquote>",
        "<p>Despite the fact the Unforgivables are one of the foulest curses in Harry Potter universe and are penalized with life imprisonment, Harry used them totally six times. Twice the Cruciatus Curse (once on Bellatrix Lestrange, which worked partially, and once on Amycus Carrow just before the Battle of Hogwarts) and four times the Imperius Curse (twice on a goblin and twice on Travers, when he, Ron and Hermione broke into goblin’s bank to stole the Horcrux). He only never used the Killing Curse. I will spare you the descriptions, so no quotes this time.</p>"
    ];

    // Display explanation
    for (var i = 0; i < 10; i++) {
        document.getElementById('explain' + i).innerHTML = quotes[i];
    }

    // Unfortunately, I had a huge problem with coloring the wrong and correct answers. I wanted to use array 'answers' and loop to do this, but I wasn't able to combine it with getElement and style.color for some reason
    document.getElementById("q1a2").style.color = "#009900";
    document.getElementById("q2a4").style.color = "#009900";
    document.getElementById("q3a1").style.color = "#009900";
    document.getElementById("q4a2").style.color = "#009900";
    document.getElementById("q5a3").style.color = "#009900";
    document.getElementById("q6a2").style.color = "#009900";
    document.getElementById("q7a3").style.color = "#009900";
    document.getElementById("q8a4").style.color = "#009900";
    document.getElementById("q9a1").style.color = "#009900";
    document.getElementById("q10a4").style.color = "#009900";

    // Focus on the bottom with scoring
    window.scrollTo(0,document.body.scrollHeight);
    return false;
}
